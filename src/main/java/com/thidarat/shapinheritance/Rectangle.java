/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.shapinheritance;

/**
 *
 * @author User
 */
public class Rectangle extends Shape  {
    
    protected  double h;
    protected  double w;
    public Rectangle(double h,double w){
        this.h=h;
        this.w=w;
    }
    @Override
    public double claArea(){
        return h*w;
    }
    @Override
    public void print(){
        System.out.println("Rectangle: "+"width: "+w+" height: "+h+" Area: "+h*w);
    }  
    
}
