/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.shapinheritance;

/**
 *
 * @author User
 */
public class Triangle extends Shape {
    private double h;
    private double b;
    public Triangle(double h ,double b){
        this.h=h;
        this.b=b;
    }
    @Override
     public double claArea(){
        return 0.5*b*h;
    }
     @Override
    public void print(){
        System.out.println("Tritangle: "+"Base: "+b+" height: "+h+" Area: "+0.5*b*h);
    }  
    
}
